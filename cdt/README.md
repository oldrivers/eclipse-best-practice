# 配置技巧

## 1、导入Makefile工程

很多时候我们的C/C++工程代码是用`makefile`来组织构建的，像各种常见的开源软件基本都是通过以下三步曲来进行编译和安装：
```shell
$ ./configure --prefix=/usr/local
$ make
$ sudo make install
```
eclipse支持直接以这种现成的代码为基础来创建一个`project`，步骤为`File->New->Makefile Project with Existing Code`，然后选择代码目录即可。

eclipse默认会在该代码目录下创建`.project`和`.cproject`文件以及 `.settings`文件夹，用于保存对该`project`的各种配置参数。

如果该代码工程的makefile有`all`和`clean`目标，则该eclipse工程不用进行额外配置就可以进行编译构建了，步骤为右键左侧`Project Explorer`中相应的工程，在弹出的菜单中选择`Build Project`或者`Clean Project`即可，这分别对应`make all`和`make clean`操作。

以上是导入Makefile工程的常规方法，不足之处在于eclipse会使用系统默认的工具链`gcc`，而我们的代码很多时候是使用交叉编译器来编译的，这导致eclipse在解析代码时查找的标准库头文件不是我们所期望的，进而导致我们浏览代码时有很多报错的红色叉叉，看着很不爽，而且还影响了eclipse分析代码调用关系的准确性，这是非常不能接受的，毕竟我们使用eclispe来浏览代码最主要的就是看中它的强大代码分析能力，否则同样免费且跨平台的`Visual Studio Code`它不香吗？

可能还有另一个不足之处就是上面提到的eclipse在代码目录下创建的那3个文件(夹)，如果我们的代码是用git或svn等版本管理工具维护的，我们可能还得把这几个文件添加到ignore配置中，否则每次看到`working directory not clean`也挺烦的，除非你把它们纳入到版本管理中，但这样的话，每次你调整该代码的eclipse工程配置，这几个文件就可能发生变化，这些变化可能是你不关心的，但它们会让你又看到`working directory not clean`之类的提示，这对强迫症者无疑是不可接受的。

经过探索，我找到了另一种简单且好用的方法来实现Makefile工程的导入，首先在eclipse所工作的`Workspace`对应的目录(一般在eclipse启动时会让你选择)下创建一个子目录，目录名一般就是将要创建的`Project`的名字，然后在该子目录下创建一个`Makefile`文件，文件内容样本如下：
```makefile
# Generic Makefile for Eclipse CDT projects
.PHONY : all clean

BUILD_DIR = /path/to/the/actual/code/build/directory

all clean:
	PS4=" " make SHELL="/bin/bash -x" -C $(BUILD_DIR) $@
```
然后按照前面讲的常规方法将该目录导入为eclipse工程，导入后可以看到它出现在eclipse主界面左侧的`Build Project`中，展开看该工程就只有一个文件：Makefile，没错，这时候再右键该工程，选择`New->Folder->Advanced->Link to alternate location (Linked Folder)->Browse...`，选择实际代码所在的目录，最后点`Finish`即可，可以重复该步骤添加更多的与该工程有关的代码目录。

这时候就可以用eclipse进行工程的编译了，方面与前面讲过的一样：右键左侧`Project Explorer`中相应的工程，在弹出的菜单中选择`Build Project`或者`Clean Project`即可。

如果你的代码用的不是默认的`all`和`clean`目标来进行编译和清理，则可以在菜单`Project->Properties->C/C++ Build->Behavior->Workbench Build Behavior`中修改`Build (Incremental build)`和`Clean`配置项的值，同时相应修改上面写的Makefile。

最后，还有最重要的一步，就是配置实际用的编译工具链及编译选项，遗憾的是不能通过eclipse界面进行配置，因为我们创建的是`Makefile Project`, eclipse将它与常规的`C/C++ Project`区别对待，提供的配置界面的可配置参数很有限，好在我发现可以直接修改eclipse的工程配置文件`.cproject`来配置很多参数，其中最重要的几个参数说明如下：
```xml
<toolChain id="cdt.managedbuild.toolchain.gnu.cross.base.614996013" name="Cross GCC" superClass="cdt.managedbuild.toolchain.gnu.cross.base">
	<option id="cdt.managedbuild.option.gnu.cross.prefix.1354503165" name="Prefix" superClass="cdt.managedbuild.option.gnu.cross.prefix" value="arm-none-linux-gnueabi-" valueType="string"/>
	<option id="cdt.managedbuild.option.gnu.cross.path.582580968" name="Path" superClass="cdt.managedbuild.option.gnu.cross.path" value="/opt/arm-none-linux-gnueabi/bin" valueType="string"/>
	<targetPlatform archList="all" binaryParser="org.eclipse.cdt.core.ELF" id="cdt.managedbuild.targetPlatform.gnu.cross.833762073" isAbstract="false" osList="all" superClass="cdt.managedbuild.targetPlatform.gnu.cross"/>
	<builder id="cdt.managedbuild.builder.gnu.cross.1295315070" keepEnvironmentInBuildfile="false" managedBuildOn="false" name="Gnu Make Builder" superClass="cdt.managedbuild.builder.gnu.cross"/>
	<tool command="clang" id="cdt.managedbuild.tool.gnu.cross.c.compiler.827454433" name="Cross GCC Compiler" superClass="cdt.managedbuild.tool.gnu.cross.c.compiler">
		<option id="gnu.c.compiler.option.misc.other.1815876975" name="Other flags" superClass="gnu.c.compiler.option.misc.other" useByScannerDiscovery="true" value="-nostdinc -nostdlib -std=c99" valueType="string"/>
		<inputType id="cdt.managedbuild.tool.gnu.c.compiler.input.1062513818" superClass="cdt.managedbuild.tool.gnu.c.compiler.input"/>
	</tool>
	<tool command="clang++" id="cdt.managedbuild.tool.gnu.cross.cpp.compiler.936545706" name="Cross G++ Compiler" superClass="cdt.managedbuild.tool.gnu.cross.cpp.compiler">
		<option id="gnu.cpp.compiler.option.misc.other.1815853674" name="Other flags" superClass="gnu.cpp.compiler.option.misc.other" useByScannerDiscovery="true" value="-nostdinc -nostdinc++ -nostdlib -std=c++11" valueType="string"/>
		<inputType id="cdt.managedbuild.tool.gnu.cpp.compiler.input.1615488515" superClass="cdt.managedbuild.tool.gnu.cpp.compiler.input"/>
	</tool>
	...
</toolChain>
```
其中：
1. `cdt.managedbuild.option.gnu.cross.prefix.1354503165`(后面的数字是随机的，以实际情况为准，后面不再赘述)的`value`用来设定交叉工具链前缀。
2. `cdt.managedbuild.option.gnu.cross.path.582580968`的`value`用来设定交叉工具链所在目录，如果工具链路径已经在`PATH`环境变量中了就可以不用配置。
3. `cdt.managedbuild.tool.gnu.cross.c.compiler.827454433`的`command`用来指定C语言工具链的名字，默认为`gcc`，如果你用的是`clang`，就可以在此设置。
4. `cdt.managedbuild.tool.gnu.cross.cpp.compiler.936545706`与上一条类似，用于C++语言。
5. `gnu.c.compiler.option.misc.other.1815876975`的`useByScannerDiscovery`用来指示eclipse在获取C语言工具链内置配置时是否要使用`value`中配置的参数，它默认为`false`，需要改成`true`。
6. `gnu.cpp.compiler.option.misc.other.1815853674`与上一条类似，用于C++语言。

正确配置以上选项后，就可以让eclipse全新编译一遍工程了，eclipse会自动解析编译过程中输出的日志，获取每个被编译的源文件的编译参数，从而正确解析该源文件(eclipse在获取到其编译参数后会在该源文件图标上多出一个扳手的标识，扳手标识的样子像钥匙)。编译结束后最好再对该工程进行一次解析刷新，步骤为：`Project->C/C++ Index->Rebuild`。

然后就可以开心浏览代码，分析代码结构了。

值得一提的是，eclipse的代码解析能力非常强，但也有它搞不定的场景，比如某头文件中使用了宏开关对同样的元素进行不同的定义，eclipse就只会解析其中一种场景，因为eclipse为了性能对头文件只进行一次解析，当解析结果作用于多个源文件上时，就可能导致其中某些文件虽然能正常编译过但IDE上却提示有错误。

又比如在某个头文件中使用typedef定义了一个类型名称`XX`，在另一个源文件中定义了同名的一个枚举值并且枚举的类型是匿名的，这时eclipse解析也会出现混乱，会在使用`XX`作为类型的地方报错误提示：`Type 'XX' could not be resolved`。

不过好在这些问题不是很常见，万一遇到也可以临时修改代码来规避，影响不是很大，总之瑕不掩瑜吧。
