color high lighting for c/c++

Example file: `eclipse.epf`

```
\!/=
/instance/org.eclipse.cdt.ui/semanticHighlighting.function.bold=false
/instance/org.eclipse.cdt.ui/semanticHighlighting.function.color=32,74,135
/instance/org.eclipse.cdt.ui/semanticHighlighting.function.enabled=true
/instance/org.eclipse.cdt.ui/semanticHighlighting.globalVariable.bold=true
/instance/org.eclipse.cdt.ui/semanticHighlighting.globalVariable.color=117,80,123
/instance/org.eclipse.cdt.ui/semanticHighlighting.globalVariable.enabled=true
/instance/org.eclipse.cdt.ui/semanticHighlighting.localVariable.color=136,138,133
/instance/org.eclipse.cdt.ui/semanticHighlighting.localVariable.enabled=true
/instance/org.eclipse.cdt.ui/semanticHighlighting.localVariableDeclaration.color=136,138,133
/instance/org.eclipse.cdt.ui/semanticHighlighting.localVariableDeclaration.enabled=true
/instance/org.eclipse.cdt.ui/semanticHighlighting.macroDefinition.color=206,92,0
/instance/org.eclipse.cdt.ui/semanticHighlighting.macroDefinition.enabled=true
/instance/org.eclipse.cdt.ui/semanticHighlighting.macroSubstitution.color=206,92,0
/instance/org.eclipse.cdt.ui/semanticHighlighting.macroSubstitution.enabled=true
/instance/org.eclipse.cdt.ui/semanticHighlighting.parameterVariable.color=117,80,123
/instance/org.eclipse.cdt.ui/semanticHighlighting.parameterVariable.enabled=true
@org.eclipse.cdt.ui=7.1.0.202011292014
file_export_version=3.0
```
